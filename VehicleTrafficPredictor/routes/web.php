<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/predict', 'PredictorController@predictViewHandler');
Route::get('/', 'PredictorController@welcomeViewHandler');
Route::post('/volume-predictor', 'PredictorController@predictActionHandler');
Route::post('/volume-extract-transform', 'PredictorController@extractAndTranformDataActionHandler');


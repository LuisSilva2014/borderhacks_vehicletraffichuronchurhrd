<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Traffic Volume Predictor </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="top-right links">
                <a href="{{ url('/predict') }}">Predict</a>
            </div>

            <div class="content">
                <div class="title m-b-md">
                    Challenge: Vehicle Traffic Predictor
                </div>

                    <h3> St Clair College - Analysis and modeling of vehicle traffic along the Huron Church Road Corridor</h3>
                <p>Develop a solution to monitor the traffic travelling along the Huron Church Rd. corridor to determine the traffic patterns affecting the volume commercial and commuter vehicles, </p>
                <p>crossing the bridge and remaining in Windsor. From this data, develop a model to predict traffic patterns for time of day and when the border opens up to non-essential travel. </p>

                <div class="links">
                    <a href="https://borderhacks.devpost.com/" target="_blank">Borderhacks Devpost</a>
                    <a href="https://www.stclaircollege.ca/programs/data-analytics-business" target="_blank">Data Analytics Business programm at StClair College</a>
                    <a href=" https://opendata.citywindsor.ca" target="_blank">Mobility data City of Windsor</a>
                </div>
                    <hr>
                <span>
                    Using Open Data from  https://opendata.citywindsor.ca
                </span>
                <br> <br>
                <span>
                   @2020 by Luis Silva & Kushal Patel  - St clair Students
                </span>

            </div>
        </div>
    </body>
</html>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" > --}}
    {{-- <link href="{{ asset('bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('js/styles/jqx.base.css') }}" rel="stylesheet">
    <link href="{{ asset('toastr.min.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('loading.css') }}"  rel="stylesheet" > --}}
    <title>Traffic Volume Predictor</title>
    <style>
        body{
            background-color:  #eeeff0;
        }
        #loader2 {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('/classifier-animation4.gif') center no-repeat #eeeff0;
            animation: 1s fadeIn;
        }
        #loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('/extracting.gif') center no-repeat #c8e9ef;
            animation: 1s fadeIn;
        }
    </style>
  </head>
  <body >
        <div id="loader2" style="display: none">  </div>
        <div id="loader" style="display: none">  </div>
        <br><br>
        <div class="container">
            <a href="/">Home</a>
            <br><br><br>
            <h1>Traffic Volume Predictor</h1>
            <h3>City of Windsor</h3>
            <br><br>
            <h6>Period range</h6>
            <div class="row">
                <div class="col-md-4 mb-3">
                  <label for="from">From</label>
                  <input type='text' class="datepicker form-control" id='from' />
                </div>
                <div class="col-md-4 mb-3">
                    <label for="to">To</label>
                    <input type='text' class="datepicker form-control" id='to' />
                </div>

                <div class="col-md-4 mb-3">
                    <label for="vehicleType">Vehicle Type</label>
                    <select class="form-control" id="vehicleType">
                        <option value="ArticulatedTruck">Commercial Traffic (ArticulatedTruck)</option>
                    </select>
                  </div>

            </div>
            <h6>Prediction values</h6>
            <div class="row">
                <div class="col-md-3 mb-3">
                  <label for="Intersection">Intersection</label>
                  <select class="form-control" id="intersectionId">
                    <option value="1">1) Dorchester Road and Huron Church Road</option>
                    <option value="2">2) Totten Street and Huron Church Road</option>
                    <option value="3">3) Malden Road and Huron Church Road</option>
                  </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="Intersection">Type of ML</label>
                    <select class="form-control" id="mlType">
                        <option value="SVC">Support Vector Classification</option>
                        <option value="SVR">Support Vector Regression</option>
                    </select>
                  </div>

                <div class="col-md-3 mb-3">
                    <label for="Intersection">Exit direction</label>
                    <select class="form-control" id="exit_id">
                        <option value="1">East</option>
                        <option value="2">West</option>
                        <option value="3">North</option>
                        <option value="4">South</option>
                    </select>
                  </div>



                  <div class="col-md-3 mb-3">
                    <label for="Intersection">Hour</label>
                    <select class="form-control" id="hour" >
                        @for ($i = 0; $i <= 23 ; $i++)
                            @if ($i ==8)
                                <option selected value='{{$i}}''>{{$i}}</option>
                            @else
                                <option value='{{$i}}''>{{$i}}</option>
                            @endif
                        @endfor
                      </select>
                  </div>
            </div>

            <div class="row">
                <div class="col-md-4 mb-3">
                    <button class="btn btn-primary btn btn-block" id="btnExtract">1) Connect Api  (Extract / Transform)</button>
                </div>
                <div class="col-md-2 mb-3">
                    <button class="btn btn-success btn btn-block" id="btnPredict">2) Predict</button>
                </div>
                <div class="col-md-6 mb-8" style="display: none" id='predictionResultContainer'>
                    <p>
                        The probable vehicles volume expected is: <input type='text' disabled style="font-size: 60px" id='predictedVolume'>
                    </p>
                        {{-- <p>Result:  <input type='text' disabled style="font-size: 20px" id='resultB22'/> </p> --}}

                        <input type='text' disabled style="font-size: 40px" id='behaviorExpected'>
                        <p>Result</p>
                        <code id='resultB'></code>
                    <br>
                    <br>
                    <div class="col-md-6 mb-3">
                        <a href="#" id='notify'>Send warning Notification</a>
                    </div>
                </div>
            </div>
            <br>
            <br> <br>
            <br>
            <br>
            <hr>
            @csrf
            <footer class="container py-5">
                <div class="row">
                  <div class="col-12 col-md">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="d-block mb-2" role="img" viewBox="0 0 24 24" focusable="false"><title>Product</title><circle cx="12" cy="12" r="10"></circle><path d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"></path></svg>
                    <small class="d-block mb-3 text-muted">© 2020</small>
                  </div>
                  <div class="col-4 col-md">
                    <h5>Authors</h5>
                    <ul class="list-unstyled text-small">
                      <li><a class="text-muted" href="#">Luis Silva</a></li>
                      <li><a class="text-muted" href="#">Kushal Patel</a></li>
                    </ul>
                  </div>
                  <div class="col-4 col-md">
                    <h5>St clair College</h5>
                    <ul class="list-unstyled text-small">
                      <li><a class="text-muted" href="#">Data Analyst for Business Program</a></li>
                    </ul>
                  </div>
                  <div class="col-4 col-md">
                    <h5>Border Hacks</h5>
                    <ul class="list-unstyled text-small">
                      <li><a class="text-muted" href="https://borderhacks.devpost.com">borderhacks.devpost.co/</a></li>
                    </ul>
                  </div>

                </div>
              </footer>



              <div id="infinite-content-loader" style="display:normal" >
                <div class="progress-effect">
                    <div class="infinite-body-div"></div>
                </div>
             </div>


              <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"> </script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"> </script> --}}
    {{-- <script src="{{ asset('bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script> --}}
    <script src="{{ asset('js/scripts/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('js/jqxcore.js') }}"></script>
    <script src="{{ asset('js/jqxdatetimeinput.js') }}"></script>
    <script src="{{ asset('js/jqxcalendar.js') }}"></script>
    <script src="{{ asset('js/globalization/globalize.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/utilities.js') }}"></script>
    {{-- <script src="{{ asset('js/jqx-all.js') }}"></script> --}}

    <script>
          $(document).ready(function () {
                // Create a jqxDateTimeInput
                let today = new Date()
                let yesterday = new Date(today)
                yesterday.setDate(yesterday.getDate() - 1);

                let lastWeek = new Date(yesterday)
                lastWeek.setDate(lastWeek.getDate() - 3);

                $("#from").jqxDateTimeInput({ formatString: "yyyy-MM-dd",  animationType: 'fade', width: '250px', height: '45px', dropDownHorizontalAlignment: 'right'});
                $("#to").jqxDateTimeInput({formatString: "yyyy-MM-dd" , animationType: 'fade', width: '250px', height: '45px', dropDownHorizontalAlignment: 'right'});
                // $set ranges
                let minDate= new Date(2020, 08, 15);
                $("#from").jqxDateTimeInput('setDate', lastWeek);
                $('#to').jqxDateTimeInput('setMaxDate', yesterday);
                $('#from').jqxDateTimeInput('setMaxDate', yesterday);


                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val()} ,  //Include laravel token to all ajax request
                    beforeSend: function() {
                        // utilities.showLoading();
                    },
                    complete: function(){
                        // utilities.hideLoading(null, true);
                    },
                });
        });
    </script>
  </body>
</html>

<?php

namespace App\Http\Controllers\Shared;
use Exception;

class HttpRequestHandler
{

    /*
    |--------------------------------------------------------------------------
    | HttpRequestHandler class
    |--------------------------------------------------------------------------
    | This helper class handles is in charge to handle http request on the targeted end points
    */
    public function httpRequestCallback($payload, $accessToken, $method, $endPointSpecified)
    {
        try {

            $authorizationHeader = NULL;
            if($accessToken)
            {
                $authorizationHeader = ('Authorization: Bearer '. $accessToken );
            }

            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL=> $endPointSpecified,
                CURLOPT_RETURNTRANSFER=>true,
                CURLOPT_ENCODING=>"",
                // CURLOPT_MAXREDIRS=>10,
                CURLOPT_TIMEOUT=>600,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST=> $method,
                CURLOPT_POSTFIELDS=> $payload,
                CURLOPT_HTTPHEADER=> array(
                    "Accept: application/json",
                    $authorizationHeader,
                    "Content-Type: application/json",
                    "User-Agent: " . config('constants.global.userAgent')
                ),
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSLVERSION=>1,
                CURLOPT_COOKIE => ''
            ));

            $response = curl_exec($ch);
            $httpCodeResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);

            if($httpCodeResponse == 200) {
                return $response;
            }
            else if($httpCodeResponse == 401) {
                 //NOTE: In case the bearer token in the product provider has expired but the user session it's still inside the application session timeout,
                // The app need to solve this restoring the bearer token automatically
                if($this->sessionHandler()) {
                    $this->httpRequestCallback($payload, $bearerTokenUsed, $method, $endPointSpecified);
                }else {
                    throw new Exception('Internal server error  trying to restore the parther session token');
                }
            }
            else if($httpCodeResponse == 403) {
                throw new Exception('The application could not connect to the partner application. The external server has forbidden this request.');
            }
            else{
                throw new Exception('Unhandled error: The application could not resolve this request');
            }
        } catch (Exception $e) {
            $_hl = new UtilitiesHelper();
            return $_hl->errorReponseHandler($e, $request);
            // http_response_code(500); //Set internal server
            // abort(500, $e);
        }
    }

}

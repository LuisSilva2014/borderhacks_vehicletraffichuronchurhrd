<?php
namespace App\Http\Controllers\Shared;
use App\Http\Controllers\Controller;
use File;
class FileHelper extends Controller
{
    public function readFile($file_path){
        if (file_exists($file_path)) {
            $env = file_get_contents($file_path);
            return $env;
        }else return null;
    }

    // And overwrite the .env with the new data
    public function createUpdateFile($data, $file_path){
            file_put_contents($file_path, $data);
    }


}

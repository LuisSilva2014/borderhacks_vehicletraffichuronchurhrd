<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Shared\HttpRequestHandler;
use Illuminate\Http\Request;
use Phpml\Regression\SVR;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;
use Phpml\Clustering\KMeans;
use Phpml\Metric\Accuracy;
use Illuminate\Support\Collection;
use Phpml\Metric\ClassificationReport;
use App\Http\Controllers\Shared\StatisticMeasure;

use Exception;
class PredictorController extends Controller
{

    public function __construct(HttpRequestHandler $_httpRequestHandler )
    {
        $this->httpRequestHandler = $_httpRequestHandler;
    }

    public function welcomeViewHandler(Request $request)
	{
        return view('welcome');
    }
     /**
     * predict  view
     * @return view
     */
    public function predictViewHandler(Request $request)
    {
        return view('predict');
    }

    public function extractAndTranformDataActionHandler()
	{
        try
        {
            ini_set('memory_limit', '512M');
            ini_set('max_execution_time ', 1200);
            ini_set('max_input_time ', 1000);
            ini_set('post_max_size ', '1000M');
            $payload=  json_decode(request()->getContent(), true);

            $api = 'https://opendata.citywindsor.ca/api/traffic?date=';
            $intersectionId =  $payload['intersectionId'];


            $dateFrom = $payload['from'] ; //-- '2020-03-13';
            $datetTo = $payload['to'] ; //'2020-03-13';
            $yearNumber =  date('Y', strtotime($dateFrom));
            $monthNumber =  date('m', strtotime($dateFrom));
            $dayFromNumber =  date('d', strtotime($dateFrom));
            $dayToNumber =  date('d', strtotime($datetTo));

            $jsonFilePath = base_path() . '/public/data/'. $payload['from'] ."T". $payload['to'] .'.json';
            $externalData = [];
            $isEmptyData = true;
            $fileExists = false;
            $fileHelper = new \App\Http\Controllers\Shared\FileHelper();
            if (file_exists( $jsonFilePath )) {
                 $__data = $fileHelper->readFile($jsonFilePath);
                 $__data = is_null($__data) ? array() : json_decode($__data, true);
                 if (!is_null($__data) && count($__data) > 0) {
                    return response()->json(['success' => false, 'dataLoaded' => true], 200);
                 }

            } else {
                //Lets collect the data
                for ($day = $dayFromNumber; $day <= $dayToNumber ; $day++) {
                    $date = $yearNumber . '-'.  $monthNumber . '-'.  $day;
                    $endpoint=  $api . $date  . '&intersectionId=' .$intersectionId ;
                    $jData = json_decode($this->httpRequestHandler->httpRequestCallback(
                        null, null, 'GET', $endpoint ) , true);


                    if(count( $jData['traffic']) >0 ){
                        $externalData[] = [
                            'data' =>  $jData['traffic'],
                            'dateGiven' => $date,
                        ];
                        $isEmptyData =false;
                    }

                }
            }

            if($isEmptyData){
                return response()->json(['success' => false, 'isEmptyData' => true], 200);
            }

            $a = [];
            //Gett all data
            for ($i=0; $i <= 23 ; $i++) {

                foreach ($externalData as $itemA) {
                    foreach ($itemA['data'] as $item) {
                        $hourRow = (int)  date('H', strtotime(str_replace('T',' ',  $item['timeStamp'] )));

                         //Only allow the commercial traffic
                        if( $item['vehicleType'] == 'ArticulatedTruck'
                        ){
                            if( $hourRow == $i){

                                //Lewts change a numeric value
                                switch ($item['exit']) {
                                    case 'E':
                                        $item['exit_id'] =1;
                                        break;
                                    case 'W':
                                        $item['exit_id'] =2;
                                        break;
                                    case 'N':
                                        $item['exit_id'] =3;
                                        break;
                                    case 'S':
                                        $item['exit_id'] =4;
                                        break;
                                }

                                $a[]=[
                                    'hourRow' => $hourRow,
                                    'qty' => $item['qty'] ,
                                    'exit_id' => $item['exit_id'] ,
                                ];
                            };
                        }
                    }
                }
            }

            //Consolidate all the hours by totals
            $collection = Collection::make($a);
            $totalByHour = [];
            for ($i=0; $i <= 23 ; $i++) {

                $totalByHour[] = [
                   $i,
                    1,
                    'totalQty' =>  $collection->where('exit_id', 1)->where('hourRow', $i)->sum('qty')
                ];

                $totalByHour[] = [
                    $i,
                    2,
                    'totalQty' => $collection->where('exit_id', 2)->where('hourRow', $i)->sum('qty')
                ];

                $totalByHour[] = [
                    $i,
                    3,
                    'totalQty' => $collection->where('exit_id', 3)->where('hourRow', $i)->sum('qty')
                ];

                $totalByHour[] = [
                    $i,
                    4,
                    'totalQty' => $collection->where('exit_id', 4)->where('hourRow', $i)->sum('qty')
                ];
            }

            if(!$fileExists){
                $fileHelper->createUpdateFile(json_encode( $totalByHour), $jsonFilePath); //Add new data
            }

            return response()->json(['success' => true], 200);
        } catch (\Exception $e) {
            throw new Exception("Error Processing Request || Message: " . $e->getMessage() . " || Line: " . $e->getLine() . " || FileName: " . $e->getFile(), 1);
        }
    }



    // public function delete_col(&$array, $key) {  //This is using by reference
    public function delete_col(&$array, $key) {
        return array_walk($array, function (&$v) use ($key) {
            unset($v[$key]);
        });
    }

	public function predictActionHandler(Request $request)
	{
        try
        {
            $payload=  json_decode(request()->getContent(), true);
            $intersectionId =  $payload['intersectionId'];
            $featuresGiven = [
                (int) $payload['hour'],
                (int) $payload['exit_id'],
            ];

            $mlType =  $payload['mlType'];

            $jsonFilePath = base_path() . '/public/data/'. $payload['from'] ."T". $payload['to'] .'.json';
            $externalData = [];
            $isEmptyData = true;
            $fileExists = false;
            $fileHelper = new \App\Http\Controllers\Shared\FileHelper();
            if (file_exists( $jsonFilePath )) {
                $__data = $fileHelper->readFile($jsonFilePath);
                $__data = is_null($__data) ? array() : json_decode($__data, true);
                if (!is_null($__data) && count($__data) > 0) {
                    //Pre fill the userMigrationResult first with latest migrated data
                    $externalData = $__data;
                    $isEmptyData = false;
                    $fileExists = true;
                }
            }

            if($isEmptyData){
                return response()->json(['success' => false, 'isEmptyData' => true], 200);
            }

            $X_commercialTrafficArray = [];
            $Y_volumeArray = [];
            $Y_volumeArray = array_column($externalData, 'totalQty');
            $X_commercialTrafficArray = $externalData;
            $this->delete_col(  $X_commercialTrafficArray , 'totalQty');

            // --------------- Core --------------
            $mlModel = null;
            switch ($mlType) {
                case 'SVR':
                    $regression = new SVR(Kernel::LINEAR);
                    $mlModel = $regression;
                    break;
                case 'SVC':
                    $classifier = new SVR(Kernel::LINEAR, $cost = 1000);
                    $mlModel = $classifier;
                    break;
            }

            $mlModel->train($X_commercialTrafficArray,  $Y_volumeArray);
            $predictedVolumeF = (float) $mlModel->predict($featuresGiven);
            $predictedVolumeF =  round($predictedVolumeF, 0);
            // --------------- End Core --------------
            $result['Behaivor Expected'] = 'Low';
            //extract data of specific column array in php
            $av = StatisticMeasure::Average($Y_volumeArray);
            $q1 = StatisticMeasure::Quartile($Y_volumeArray, 0.25);
            $q2 = StatisticMeasure::Quartile($Y_volumeArray, 0.50);
            $q3 = StatisticMeasure::Quartile($Y_volumeArray, 0.65);
            $q4 = StatisticMeasure::Quartile($Y_volumeArray, 0.75);
            $result['1st Quartile'] = $q1 ;
            $result['2sd Quartile'] = $q2 ;
            $result['3.1rd Quartile'] = $q3 ;
            $result['3.2rd Quartile'] = $q4 ;
            $result['Hour #'] = $payload['hour'] ;
            if($predictedVolumeF <=  $q1)
            {
                $result['behaviorExpected'] = 'Light';
                $result['color'] = 'green';
            }else if($predictedVolumeF  <=   $q2)
            {
                $result['behaviorExpected'] = 'Moderate';
                $result['color'] = 'blue';
            }else if($predictedVolumeF  <=   $q3){
                $result['color'] = 'orange';
                $result['behaviorExpected'] = 'High';
            }else if($predictedVolumeF  <=   $q4){
                $result['color'] = 'red';
                $result['behaviorExpected'] = 'Heavy';
            }
            return response()->json(['success' => true, 'predictedVolumeF' => $predictedVolumeF, 'result' => $result], 200);

        } catch (\Exception $e) {
            throw new Exception("Error Processing Request || Message: " . $e->getMessage() . " || Line: " . $e->getLine() . " || FileName: " . $e->getFile(), 1);
        }
    }
}

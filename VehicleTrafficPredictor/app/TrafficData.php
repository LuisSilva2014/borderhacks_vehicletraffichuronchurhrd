<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrafficData extends Model
{
    protected $table = 'traffic_data';
    // protected $fillable = ['vehicleType', 'exit', 'entrance', 'date', 'hour', 'qty', 'timeStamp', 'isDaylightSavingsTime', 'exit_id'];
}


(function (utilities, $, undefined) {

    utilities.lastJsErrorEmailSent = 0;
    utilities.limitToResentInSec = 600 ;  //10 min

    utilities.showLoading = function (i ) {
        document.body.style.cursor='wait';
        if(i == 2){
            $("#loader2").fadeIn(400);
        }else{
            $("#loader").fadeIn(400);
        }
    }

    utilities.displayToasterMessage = function (XHR, title, type, options) {
        toastr.clear();
        options.progressBar = true;
        options.preventDuplicates  = true;
        if (options){
            toastr[type]( XHR.message, title, options);
        }else  toastr[type]( XHR.message, title);
    }

    utilities.errorHandlerResponse = function (XHR, _time, title) {
        let options = {  timeOut: _time ? _time : 10000 , fadeOut: _time ? _time : 10000 };
        options.progressBar = true;
        options.preventDuplicates  = true;
        toastr['error'](XHR, title || '', options );
    }
    $("#notify").click(function () {
        utilities.displayToasterMessage({ message: 'Comming soon! We are gonna be able to notfiy the city personal in real time! '}, 'Notification', 'info', {
            timeOut: 10000,
            fadeOut: 10000,
        });
    });

    $("#btnPredict").click(function () {

          // To set two dates to two variables
            var date1 = new Date( document.getElementById('from').value);
            var date2 = new Date( document.getElementById('to').value);
            // To calculate the time difference of two dates
            var Difference_In_Time = date2.getTime() - date1.getTime();
            // To calculate the no. of days between two dates
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            if(Difference_In_Days > 10){
                utilities.displayToasterMessage({ message: 'Sorry, out server memory can not support more than 10 days of computing processing'}, '', 'error', {
                    timeOut: 5000,
                    fadeOut: 5000,
                });
                return false;
            }
            utilities.showLoading(2);
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val()} ,
                url: '/volume-predictor',
                contentType: "application/json",
                data: JSON.stringify({
                    intersectionId :  document.getElementById('intersectionId').value,
                    exit_id :  document.getElementById('exit_id').value,
                    hour : document.getElementById('hour').value,
                    from : document.getElementById('from').value,
                    to : document.getElementById('to').value,
                    mlType : document.getElementById('mlType').value,
                }),
                success: function (data) {

                    if (data.success) {
                        document.getElementById('predictedVolume').value = data.predictedVolumeF;
                        document.getElementById('behaviorExpected').value = data.result.behaviorExpected;
                        document.getElementById('behaviorExpected').style.color = data.result.color;
                        document.getElementById('resultB').innerHTML  = JSON.stringify(data.result, null, 4); //Pretty json formatt
                        // document.getElementById('accuracy').value = data.result.accuracy;

                        $("#predictionResultContainer").show(1000);
                        utilities.displayToasterMessage({ message: 'The data have been Tidy, Transformed, and Predicted successfully'}, '', 'success', {
                            timeOut: 5000,
                            fadeOut: 5000,
                        });

                    }else if (data.isEmptyData) {
                        utilities.displayToasterMessage({ message: 'There no values from the City of Windsor for the given dates'}, '', 'error', {
                            timeOut: 5000,
                            fadeOut: 5000,
                        });
                        $("#predictionResultContainer").hide(1000);
                    }

                    utilities.hideLoading(2);

                },
                error: function (respose, status, error) {
                    utilities.displayToasterMessage({ message: respose.responseText }, 'Error', 'error', {
                        timeOut: 10000,
                        fadeOut: 10000,
                    });
                    console.error(respose);
                    console.trace();
                    utilities.hideLoading(2);
                    $("#predictionResultContainer").hide();
                }
            }
        );
    });

    $("#btnExtract").click(function () {

        // To set two dates to two variables
          var date1 = new Date( document.getElementById('from').value);
          var date2 = new Date( document.getElementById('to').value);
          // To calculate the time difference of two dates
          var Difference_In_Time = date2.getTime() - date1.getTime();
          // To calculate the no. of days between two dates
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          if(Difference_In_Days > 10){
              utilities.displayToasterMessage({ message: 'Sorry, our server memory can not support more than 9 days of computing processing'}, '', 'error', {
                  timeOut: 5000,
                  fadeOut: 5000,
              });
              return false;
          }
          utilities.showLoading();
          $.ajax({
              type: 'POST',
              headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val()} ,
              url: '/volume-extract-transform',
              contentType: "application/json",
              data: JSON.stringify({
                  intersectionId :  document.getElementById('intersectionId').value,
                  exit_id :  document.getElementById('exit_id').value,
                  hour : document.getElementById('hour').value,
                  from : document.getElementById('from').value,
                  to : document.getElementById('to').value,
                  mlType : document.getElementById('mlType').value,
              }),
              success: function (data) {

                  if (data.success) {
                      utilities.displayToasterMessage({ message: 'The data have been Extracted and Transformed successfully'}, '', 'success', {
                          timeOut: 5000,
                          fadeOut: 5000,
                      });

                  }else if (data.isEmptyData) {
                      utilities.displayToasterMessage({ message: 'There no values from the City of Windsor for the given dates'}, '', 'error', {
                          timeOut: 5000,
                          fadeOut: 5000,
                      });
                    }else if (data.dataLoaded) {
                        utilities.displayToasterMessage({ message: 'The data for this range have been already prepared. Please procced to Predict the Given value'}, 'Great', 'info', {
                            timeOut: 5000,
                            fadeOut: 5000,
                        });
                    }

                  utilities.hideLoading();

              },
              error: function (respose, status, error) {
                  utilities.displayToasterMessage({ message: respose.responseText }, 'Error', 'error', {
                      timeOut: 10000,
                      fadeOut: 10000,
                  });
                  console.error(respose);
                  console.trace();
                  utilities.hideLoading();
              }
          }
      );
  });
    utilities.hideLoading = function () {
        document.body.style.cursor='default';
        $("#loader").fadeOut(300);
        $("#loader2").fadeOut(300);
    }


}(window.utilities = window.utilities || {}, jQuery));


